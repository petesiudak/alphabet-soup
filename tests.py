import pytest

from alphabet_soup import issubset


@pytest.mark.parametrize(
    "message, bowl, expected_result", [
        ("", "", False),
        ("a", "", False),
        ("", "a", False),
        ("msg", "abc", False),
        ("abcd", "abc", False),
        ("This is a random message", "JgCxOfAhXyMhIxIlJoBtBkTxFgTuKRKd", False),
        ("a", "a", True),
        ("a", "ab", True),
        ("abc", "abc", True),
        ("message", "sabcgefghmnopste", True),
        ("I am Pete", "abIcdeftgmhePyz", True),
        ("Bob knows you", "AxPoBkOnHgblSmAgGnfOtSnApTpsGygNwjIkmUeIoqou", True),
        ("short", "horstabIcdeftgmhePyzsabcgefghmnopste", True),
    ]
)
def test_issubset_returns_expected_result(
    message: str,
    bowl: str,
    expected_result: bool
):
    assert issubset(message, bowl) is expected_result

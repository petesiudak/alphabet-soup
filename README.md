## Alphabet Soup

Everyone loves alphabet soup.  And of course, you want to know if you can construct a message from the letters found in your bowl. Your Task: Write a function that takes as input two strings:

1. The message you want to write.
2. All the letters found in your bowl of alphabet soup.

Assumptions:

1. It may be a very large bowl of soup containing many letters.
2. There is no guarantee that each letter occurs a similar number of times - indeed some letters might be missing entirely.
3. The letters are ordered randomly.

The function should determine if you can write your message with the letters found in your bowl of soup. The function should return True or False accordingly. Try to make your function efficient.  Please use Big-O notation to explain how long it takes your function to run in terms of the length of your message (m) and the number of letters in your bowl of soup (s).

#### To run a program use one of the following commands:

1. ```python main.py -m "This is my message" -b "Thisismybowlofletters```.
2. ```python main.py -r 100```- it generates a random string of 100 letters.
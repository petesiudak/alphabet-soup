import collections
import random


def issubset(message: str, bowl: str) -> bool:
    """Time complexity of the function is O(n+m) where n is
    the length of the given message and m is the length of the bowl,
    which finally gives O(max(n,m)) => O(N).

    :param message: A message to be written.
    :param bowl: All the letters in a bowl.
    :return: A bool value indicating whether the message
             can be written with the letters found in the bowl.
    """
    if not message or not bowl:  # O(1)
        return False

    message = message.strip().replace(" ", "")  # O(len(message))
    bowl = bowl.strip().replace(" ", "")  # O(len(bowl))
    if len(message) > len(bowl):  # O(1)
        return False

    message_count = collections.Counter(message)  # O(len(message))
    total_counts = sum(message_count.values())  # O(len(message_count))

    for letter in bowl:  # O(len(bowl))
        if letter in message_count:  # O(1)
            total_counts -= 1  # O(1)
        if total_counts == 0:  # O(1)
            return True
    return False


def randstr(n: int) -> str:
    """It generates a random string of length n.
    :param n: The length of the random string.
    :return: A random string.
    """
    letters = []
    for i in range(n):
        random_letters = (
            chr(random.randint(65, 90)),
            chr(random.randint(97, 122))
        )
        letters.append(random_letters[random.randint(0, 1)])
    return "".join(letters)

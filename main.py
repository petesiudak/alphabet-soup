import argparse
import sys

from alphabet_soup import issubset, randstr, read_file, issubset_v2


def cli():
    parser = argparse.ArgumentParser(
        description="Check whether you can construct "
                    "a message from letters found in your bowl."
    )
    parser.add_argument(
        "-m",
        "--message",
        type=str,
        help="A message to be written.",
        required=False
    )
    parser.add_argument(
        "-b",
        "--bowl",
        type=str,
        help="A bowl of alphabet soup.",
        required=False
    )
    parser.add_argument(
        "-r",
        "--randstr",
        type=int,
        help="Generate a random string of n letters",
        required=False
    )
    args = parser.parse_args()
    if args.randstr:
        sys.stdout.write(randstr(args.randstr))
        sys.exit()
    sys.stdout.write(str(issubset(args.message, args.bowl)))


if __name__ == "__main__":
    cli()
